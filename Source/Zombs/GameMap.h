// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class ZOMBS_API GameMap
{
public:
	int width;
	int height;
	int **map;
	GameMap();
	GameMap(int _width, int _height);
	~GameMap();
	void setAll(int value);
	void print();
};