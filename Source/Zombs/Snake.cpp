// Fill out your copyright notice in the Description page of Project Settings.

#include "Zombs.h"
#include "Snake.h"
#include <stdlib.h>
#include <string>
#include <sstream>

using namespace std;

Snake::Snake()
{

}

Snake::Snake(GameMap *_gameMap, bool _hasExit)
{
	this->gameMap = _gameMap;
    this->hasExit = _hasExit;
}

Snake::~Snake()
{
    
}

void Snake::moveRandom(int pr, int px, int py)
{
	if (isEdge(px,py))
	{
		this->gameMap->map[px][py] = 2;
		hasCreatedExit = true;
	}
	else
		this->gameMap->map[px][py] = 0;

	int r = rand() % 4;
	// whether or not it should move in the same direction
	if (rand() % STICKINESS == 0)
		r = pr;

	int numSpawned = 0;
	for (int i=0; i<4; i++)
	{
		int tx = px;
		int ty = py;

		if (r == 0)
			tx++;
		if (r == 1)
			tx--;
		if (r == 2)
			ty++;
		if (r == 3)
			ty--;

		int likelyhood = rand() % (numSpawned*BRANCH_RATE + 1);

//			Debug.Log ("r: " + r);
//			Debug.Log ("canspawn("+px+","+py+"): " + canSpawn(px,py));
		if (canSpawn(tx, ty) && likelyhood == 0)
		{
			// new Snake(this->gameMap, px, py);
			// snake->moveRandom(r);
			// delete snake;
			moveRandom(r, tx, ty);
			numSpawned++;
		}

		r++;
		r = r%4;
	}
}

bool Snake::canSpawn(int px, int py)
{
	if (px < 0 || px >= gameMap->width || py < 0 || py >= this->gameMap->height)
		return false;

	if (this->gameMap->map[px][py] != 1)
		return false;

	int count = 0;

	// x
	if (px - 1 >= 0)
		count += this->gameMap->map[px-1][py];
    else if (!hasCreatedExit)
		count++;
	if (px + 1 < this->gameMap->width)
		count += this->gameMap->map[px+1][py];
	else if (!hasCreatedExit)
		count++;

	// y
	if (py - 1 >= 0)
		count += this->gameMap->map[px][py-1];
	else if (!hasCreatedExit)
		count++;
	if (py + 1 < this->gameMap->height)
		count += this->gameMap->map[px][py+1];
	else if (!hasCreatedExit)
		count++;

	// diagonals
	if (px - 1 >= 0)
	{
		if (py - 1 >= 0)
			count += this->gameMap->map[px-1][py-1];
		if (py + 1 < this->gameMap->width)
			count += this->gameMap->map[px-1][py+1];
	}
	if (px + 1 < this->gameMap->width)
	{
		if (py - 1 >= 0)
			count += this->gameMap->map[px+1][py-1];
		if (py + 1 < this->gameMap->height)
			count += this->gameMap->map[px+1][py+1];
	}
    
    bool ret = 8-count < 3;
    
    if (isEdge(px,py) && !hasCreatedExit)
    {
        UE_LOG(LogTemp, Log, TEXT("created Exit"));
        ostringstream oss;
		oss << px << py;
		UE_LOG(LogTemp, Log, TEXT("%s"), UTF8_TO_TCHAR(oss.str().c_str()));
		ret = true;
	}

	return ret;
}

bool Snake::isEdge(int px, int py)
{
	return px == 0 || py == 0 || px == this->gameMap->width-1 || py == this->gameMap->height-1;
}