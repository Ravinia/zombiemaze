// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Zombs.h"
#include "ZombsGameMode.h"
#include "ZombsPlayerController.h"
#include "ZombsCharacter.h"

AZombsGameMode::AZombsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AZombsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}