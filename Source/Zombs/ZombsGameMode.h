// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ZombsGameMode.generated.h"

UCLASS(minimalapi)
class AZombsGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AZombsGameMode();
};



