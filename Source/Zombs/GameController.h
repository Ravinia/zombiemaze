// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameMap.h"
#include "GameController.generated.h"


UCLASS()
class ZOMBS_API AGameController : public AActor
{
	GENERATED_BODY()

private:
    const int boxSize = 400;
	const int SIDES = 4;
	GameMap* gameMap;
    int MAP_SIZE = 20;
    int initialZombieCount = 10;
    int zombieSpawnRate = 200; // lower is faster
	void createMaze(FVector startPos);
    void spawnZombie();

    
	
public:
    
    TSubclassOf<class AActor> cubeClass;
    TSubclassOf<class AActor> zombieClass;
    TSubclassOf<class AActor> winTileClass;
    
    // Sets default values for this actor's properties
	AGameController();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

};