// Fill out your copyright notice in the Description page of Project Settings.

#include "Zombs.h"
#include "GameMap.h"
#include <stdlib.h>
#include <string>
#include <sstream>

using namespace std;

GameMap::GameMap()
{

}

GameMap::GameMap(int _width, int _height)
{
	this->width = _width;
	this->height = _height;
	this->map = new int*[width];
	for (int i=0; i<height; i++)
		map[i] = new int[height];
}

GameMap::~GameMap()
{
}

void GameMap::setAll(int value)
{
	for (int i=0; i<this->width; i++)
	{
		for (int j=0; j<this->height; j++)
		{
			this->map[i][j] = value;
		}
	}
}

void GameMap::print()
{
	ostringstream oss;
	for (int i=0; i<this->width; i++)
	{
		for (int j=0; j<this->height; j++)
		{
			
			oss << this->map[i][j] << " ";
		}
		oss << endl;
	}
	UE_LOG(LogTemp, Log, TEXT("%s"), UTF8_TO_TCHAR(oss.str().c_str()));
}