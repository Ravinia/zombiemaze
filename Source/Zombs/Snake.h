// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameMap.h"

/**
 * 
 */
class ZOMBS_API Snake
{

private:
	GameMap *gameMap;
    bool hasExit = false;
    bool hasCreatedExit = false;

	const int BRANCH_RATE = 2; // lower is faster
	const int STICKINESS = 3; // larger means harder to change direction

	bool canSpawn(int px, int py);

public:
	Snake();
	Snake(GameMap* gameMap, bool _hasExit);
	~Snake();
	void moveRandom(int pr, int px, int py);
	bool Snake::isEdge(int px, int py);

};