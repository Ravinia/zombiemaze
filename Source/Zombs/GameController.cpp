// Fill out your copyright notice in the Description page of Project Settings.

#include "Zombs.h"
#include "GameController.h"
#include "Snake.h"
#include "Block.h"
#include "Engine/Blueprint.h"
#include <stdlib.h>
#include <string>

using namespace std;

// Sets default values
AGameController::AGameController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    static ConstructorHelpers::FObjectFinder<UBlueprint> CubeBlueprint(TEXT("Blueprint'/Game/Box.Box'"));
    if (CubeBlueprint.Object) {
        cubeClass = (UClass*)CubeBlueprint.Object->GeneratedClass;
    }
    
    static ConstructorHelpers::FObjectFinder<UBlueprint> ZombieBlueprint(TEXT("Blueprint'/Game/TopDownCPP/Blueprints/ZombieBlueprint.ZombieBlueprint'"));
    if (ZombieBlueprint.Object) {
        zombieClass = (UClass*)ZombieBlueprint.Object->GeneratedClass;
    }

    static ConstructorHelpers::FObjectFinder<UBlueprint> WinTileBlueprint(TEXT("Blueprint'/Game/WinTile.WinTile'"));
    if (WinTileBlueprint.Object) {
        winTileClass = (UClass*)WinTileBlueprint.Object->GeneratedClass;
    }

    gameMap = new GameMap(MAP_SIZE, MAP_SIZE);
}

// Called when the game starts or when spawned
void AGameController::BeginPlay()
{
	Super::BeginPlay();

	FVector *startPos = new FVector(MAP_SIZE/2, MAP_SIZE/2, 0);
	createMaze(*startPos);
    for (int i=0; i<initialZombieCount; i++)
        spawnZombie();
}

// Called every frame
void AGameController::Tick( float DeltaTime )
{
    Super::Tick( DeltaTime );
    
    if (rand() % zombieSpawnRate == 0)
    {
        spawnZombie();
    }
}

void AGameController::spawnZombie()
{
    UWorld* const World = GetWorld();
    if (World)
    {
        int mx;
        int my;
        while (true)
        {
            mx = rand() % MAP_SIZE;
            my = rand() % MAP_SIZE;
            if (gameMap->map[mx][my] == 0)
                break;
        }

        int tx = (mx - MAP_SIZE/2)*boxSize;
        int ty = (my - MAP_SIZE/2)*boxSize;
        FVector location = FVector(tx, ty, 10);
        FRotator rotation(0, 0, 0);
        AActor* zombie = World->SpawnActor<AActor>(zombieClass, location, rotation);
    }
}

void AGameController::createMaze(FVector startPos)
{
	UE_LOG(LogTemp, Log, TEXT("creating maze"));

	gameMap->setAll(1);
	gameMap->print();
    
	Snake *snake = new Snake(gameMap, true);
	snake->moveRandom(rand() % 4, MAP_SIZE/2, MAP_SIZE/2);
	delete snake;
	gameMap->print();
    
    UWorld* const World = GetWorld();
    if (World){
        for (int i=0; i<gameMap->width; i++)
        {
            for (int j=0; j<gameMap->height; j++)
            {
                FVector location = FVector((i-gameMap->width/2)*boxSize, (j-gameMap->height/2)*boxSize, 0);
                FRotator rotation(0, 0, 0);
                if (gameMap->map[i][j] == 1)
                {
                    AActor* createdCube = World->SpawnActor<AActor>(cubeClass, location, rotation);
                }
                else if (gameMap->map[i][j] == 2)
                {
                    AActor* createdWinTile = World->SpawnActor<AActor>(winTileClass, location, rotation);
                }
            }
        }
    }
}